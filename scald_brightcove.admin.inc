<?php
/**
 * @file
 * Provides admin form for Brightcove's Scald Provider.
 */

/**
 * Defines Brightcove settings form.
 */
function scald_brightcove_settings_form() {
  $form = array();

  // Needed to validate de vidéos.
  $form['scald_brightcove_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Brightcove token'),
    '#default_value' => variable_get('scald_brightcove_token', ''),
    '#required' => TRUE,
  );

  // Default player for video imported from there Id.
  $form['scald_brightcove_default_player'] = array(
    '#type' => 'fieldset',
    '#title' => t('Brightcove default player.'),
  );

  $form['scald_brightcove_default_player']['scald_brightcove_default_player_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Player Id.'),
    '#default_value' => variable_get('scald_brightcove_default_player_id', ''),
  );

  $form['scald_brightcove_default_player']['scald_brightcove_default_player_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Player Key.'),
    '#default_value' => variable_get('scald_brightcove_default_player_key', ''),
  );

  return system_settings_form($form);
}


/**
 * Defines the import settings form.
 */
function scald_brightcove_imports_form() {
  // ...
}

/**
 * Handles the submission of the form that adds a new import.
 */
function scald_brightcove_imports_form_add($form, &$form_state) {
  // ...
}

/**
 * Handles the submission of the whole form.
 */
function scald_brightcove_imports_form_submit($form, &$form_state) {
  // ...
}
