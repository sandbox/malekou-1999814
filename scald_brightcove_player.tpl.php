<?php
/**
 * @file
 * Default theme implementation for the Scald Brightcove player.
 *
 * @param $vars['video_id']
 * @param $vars['video_bg_color']
 * @param $vars['video_width']
 * @param $vars['video_height']
 * @param $vars['player_id']
 * @param $vars['player_key']
 * @param $vars['video_is_ui']
 * @param $vars['video_dynamic_streaming']
 */
?>
<!-- Start of Brightcove Player -->

<div style="display:none"></div>

<!--
By use of this code snippet, I agree to the Brightcove Publisher T and C
found at https://accounts.brightcove.com/en/terms-and-conditions/.
-->

<script language="JavaScript" type="text/javascript" src="http://admin.brightcove.com/js/BrightcoveExperiences.js"></script>

<object id="myExperience<?php print $vars['video_id']; ?>" class="BrightcoveExperience">
  <param name="bgcolor" value="<?php print $vars['video_bg_color']; ?>" />
  <param name="width" value="<?php print $vars['video_width']; ?>" />
  <param name="height" value="<?php print $vars['video_height']; ?>" />
  <param name="playerID" value="<?php print $vars['player_id']; ?>" />
  <param name="playerKey" value="<?php print $vars['player_key']; ?>" />
  <param name="isVid" value="true" />
  <param name="isUI" value="<?php print $vars['video_is_ui']; ?>" />
  <param name="dynamicStreaming" value="<?php print $vars['video_dynamic_streaming']; ?>" />
  <param name="@videoPlayer" value="<?php print $vars['video_id']; ?>" />
</object>

<!--
This script tag will cause the Brightcove Players defined above it to be created as soon
as the line is read by the browser. If you wish to have the player instantiated only after
the rest of the HTML is processed and the page load is complete, remove the line.
-->
<script type="text/javascript">
  brightcove.createExperiences();
</script>

<!-- End of Brightcove Player -->
