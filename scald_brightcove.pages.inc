<?php
/**
 * @file
 * Contains form handlers for the Brightcove search form.
 */

/**
 * Generates the search and search results form.
 */
function scald_brightcove_search_form($form, &$form_state) {
  // ..
}

/**
 * Handles search terms form submission.
 */
function scald_brightcove_search_form_search_submit($form, &$form_state) {
  // ...
}

/**
 * Handlers import form submission.
 */
function scald_brightcove_search_form_submit($form, &$form_state) {
  // ...
}

/**
 * Themes the results table.
 */
function theme_scald_brightcove_search_results_table($variables) {
  // ...
}
